var exec = require('child_process').exec;
var fs = require('fs');
var moment = require('moment');
const ObjectsToCsv = require('objects-to-csv');

function doTest() {
    return new Promise((resolve,reject)=>{
        var command = "speedtest -f json"
        child = exec(command,
           function (error, stdout, stderr) {
              console.log(stdout);
              resolve(stdout);
              if (stderr !== null) {
                  reject(stderr);
              }
           });
    });
}


function testRun() {
    doTest().then((stdout)=>{
        if (stdout) {
            var data = JSON.parse(stdout);
            if (!data.packetLoss) {
                data.packetLoss=0;
            } 
            if (data.error) {
                var result = {
                        timestamp: new moment().format("DD/MM/YYYY HH:mm"),
                        isp:"Vodafone",
                        servername: "Test Server unreachable",
                        location:  "Test Server unreachable",
                        country:  "Test Server unreachable",
                        host:  "Test Server unreachable",
                        packetLoss:100,
                        latency:0,
                        jitter:0,
                        download:0,
                        upload: 0,
                        resulturl:data.error
                    }
            }
            else {
                    var result ={
                        timestamp: new moment().format("DD/MM/YYYY HH:mm"),
                        isp:data.isp,
                        servername: data.server.name,
                        location: data.server.location,
                        country: data.server.country,
                        host: data.server.host,
                        packetLoss:data.packetLoss,
                        latency:data.ping.latency,
                        jitter:data.ping.jitter,
                        download: data.download.bandwidth/125000,
                        upload: data.upload.bandwidth/125000,
                        resulturl:data.result.url
                    }
            }
        }
        else if (!stdout) {
            var result ={
                timestamp: new moment().format("DD/MM/YYYY HH:mm"),
                isp:"Vodafone",
                servername: "Test Server unreachable",
                location:  "Test Server unreachable",
                country:  "Test Server unreachable",
                host:  "Test Server unreachable",
                packetLoss:100,
                latency:0,
                jitter:0,
                download:0,
                upload: 0,
             }
		if (stdout.error) {
			result.resulturl = stdout.error;
		}	
		result.resulturl = "Test Server unreachable";	
        }
        console.log(result);
        console.log("test done - writing");
        new ObjectsToCsv([result]).toDisk('./speedtest.csv', { append: true, bom:true });
    }).catch((stderr)=>{
        console.log(stderr);
    }); 
  } 
  if (!process.argv[2] || isNaN(process.argv[2])) {
      console.log("Usage: node index.js <interval in minutes> (min 1)");
  } 
  
  else {
    console.log ("Running Speed tests every " +process.argv[2] + " minutes - Press Ctrl+C to exit");
    setInterval(testRun, process.argv[2] *60000);
  }

