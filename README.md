
#Pre-requisites

You will need to have speedtest cli installed:
mac:
-brew tap teamookla/speedtest

-brew update

-brew install speedtest --force
windows:
https://bintray.com/ookla/download/download_file?file_path=ookla-speedtest-1.0.0-win64.zip

unzip to working directory

You will need to run speedtest once manually in order to accept the License conditions.

#usage

-npm install
-node index.js <test interval (mins)>

#output
logs will be output to speedtest.csv ready for import into Excel/Openoffice/Google Sheets